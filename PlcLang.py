from stringWithArrows import *
import string
import os
import math

DIGITS = '0123456789'
LETTERS = string.ascii_letters
LETTERS_DIGITS = LETTERS + DIGITS


# --------------------------- Error Handeling ------------------------------
class Error:
    def __init__(self, posStart, posEnd, errorName, details):
        self.posStart = posStart
        self.posEnd = posEnd
        self.errorName = errorName
        self.details = details

    def as_string(self):
        result = f'{self.errorName}: {self.details}'
        result += f'\nFile {self.posStart.fileName}, Line {self.posStart.ln + 1}'
        result += '\n\n' + \
            stringWithArrows(self.posStart.fileTxt, self.posStart, self.posEnd)
        return result


class IllegalCharError(Error):
    def __init__(self, posStart, posEnd, details):
        super().__init__(posStart, posEnd, 'Illegal Character', details)


class ExpectedCharError(Error):
    def __init__(self, posStart, posEnd, details):
        super().__init__(posStart, posEnd, 'Expected Character', details)


class InvalidSyntaxError(Error):
    def __init__(self, posStart, posEnd, details=""):
        super().__init__(posStart, posEnd, 'Invalid Syntax', details)


class RunTimeError(Error):
    def __init__(self, posStart, posEnd, details, context):
        super().__init__(posStart, posEnd, 'Runtime Error', details)
        self.context = context

    def as_string(self):
        result = self.generateTraceBack()
        result += f'{self.errorName}: {self.details}'
        result += '\n\n' + \
            stringWithArrows(self.posStart.fileTxt, self.posStart, self.posEnd)
        return result

    def generateTraceBack(self):
        result = ''
        pos = self.posStart
        context = self.context

        while context:
            result = f'  File {pos.fileName}, line {str(pos.ln + 1)}, in {context.displayName}\n' + result
            pos = context.parentEntryPosition
            context = context.parent
        return 'TraceBack (mos recent call last):\n' + result


# --------------------------- Position ------------------------------
class Position:
    def __init__(self, idx, ln, col, fileName, fileTxt):
        self.idx = idx
        self.ln = ln
        self.col = col
        self.fileName = fileName
        self.fileTxt = fileTxt

    def advance(self, currentChar=None):
        self.idx += 1
        self.col += 1

        if currentChar == '\n':
            self.ln += 1
            self.col = 0
        return self

    def copy(self):
        return Position(self.idx, self.ln, self.col, self.fileName, self.fileTxt)


TT_INT = 'INT'
TT_FLOAT = 'FLOAT'
TT_STRING = 'STRING'
TT_PLUS = 'PLUS'
TT_MINUS = 'MINUS'
TT_MUL = 'MUL'
TT_DIV = 'DIV'
TT_POW = 'POW'
TT_LPAREN = 'LPAREN'
TT_RPAREN = 'RPAREN'
TT_IDENTIFIER = 'IDENTIFIER'
TT_KEYWORD = 'KEYWORD'
TT_EQ = 'EQ'
TT_EE = 'EE'
TT_NE = 'NE'
TT_LT = 'LT'
TT_GT = 'GT'
TT_LTE = 'LTE'
TT_GTE = 'GTE'
TT_COMMA = 'COMMA'
TT_ARROW = 'ARROW'
TT_EOF = 'EOF'
TT_NEWLINE = 'NEWLINE'
TT_RSQUARE = 'RIGHTBRACKET'
TT_LSQUARE = 'LEFTBRACKET'
KEYWORDS = [
    'var',
    'and',
    'or',
    'not',
    'if',
    'then',
    'elif',
    'else',
    'for',
    'to',
    'step',
    'while',
    'fun',
    'then',
    'end',
    'return',
    'continue',
    'break'
]


# --------------------------- Token ------------------------------
class Token:
    def __init__(self, type_, value=None, posStart=None, posEnd=None):
        self.type = type_
        self.value = value
        if posStart:
            self.posStart = posStart.copy()
            self.posEnd = posStart.copy()
            self.posEnd.advance()
        if posEnd:
            self.posEnd = posEnd

    def matches(self, type_, value):
        return self.type == type_ and self.value == value

    def __repr__(self):
        if self.value:
            return f'{self.type}:{self.value}'
        return f'{self.type}'


# --------------------------- Lexer ------------------------------
class Lexer:
    def __init__(self, fileName, text):
        self.fileName = fileName
        self.text = text
        self.pos = Position(-1, 0, -1, fileName, text)
        self.currentChar = None
        self.advance()

    def advance(self):
        self.pos.advance(self.currentChar)
        self.currentChar = self.text[self.pos.idx] if self.pos.idx < len(
            self.text) else None

    def makeTokens(self):
        tokens = []

        while self.currentChar != None:
            if self.currentChar in ' \t':
                self.advance()
            elif self.currentChar == '#':
                self.skip_comment()
            elif self.currentChar in DIGITS:
                tokens.append(self.makeNumber())
            elif self.currentChar in ';\n':
                tokens.append(Token(TT_NEWLINE, posStart=self.pos))
                self.advance()
            elif self.currentChar in LETTERS:
                tokens.append(self.makeIdentifier())
            elif self.currentChar == '"':
                tokens.append(self.makeString())
            elif self.currentChar == '+':
                tokens.append(Token(TT_PLUS, posStart=self.pos))
                self.advance()
            elif self.currentChar == '-':
                tokens.append(self.makeMinusOrArrow())
            elif self.currentChar == '*':
                tokens.append(Token(TT_MUL, posStart=self.pos))
                self.advance()
            elif self.currentChar == '/':
                tokens.append(Token(TT_DIV, posStart=self.pos))
                self.advance()
            elif self.currentChar == '^':
                tokens.append(Token(TT_POW, posStart=self.pos))
                self.advance()
            elif self.currentChar == '(':
                tokens.append(Token(TT_LPAREN, posStart=self.pos))
                self.advance()
            elif self.currentChar == ')':
                tokens.append(Token(TT_RPAREN, posStart=self.pos))
                self.advance()
            elif self.currentChar == '[':
                tokens.append(Token(TT_LSQUARE, posStart=self.pos))
                self.advance()
            elif self.currentChar == ']':
                tokens.append(Token(TT_RSQUARE, posStart=self.pos))
                self.advance()
            elif self.currentChar == '!':
                token, error = self.makeNotEquals()
                if error:
                    return [], error
                tokens.append(token)
            elif self.currentChar == '=':
                tokens.append(self.makeEquals())
            elif self.currentChar == '<':
                tokens.append(self.makeLessThan())
            elif self.currentChar == '>':
                tokens.append(self.makeGreaterThan())
            elif self.currentChar == ',':
                tokens.append(Token(TT_COMMA, posStart=self.pos))
                self.advance()
            else:
                posStart = self.pos.copy()
                char = self.currentChar
                self.advance()
                return [], IllegalCharError(posStart, self.pos, "'" + char + "'")

        tokens.append(Token(TT_EOF, posStart=self.pos))
        return tokens, None

    def makeNumber(self):
        numString = ''
        dotCount = 0
        posStart = self.pos.copy()

        while self.currentChar != None and self.currentChar in DIGITS + '.':
            if self.currentChar == '.':
                if dotCount == 1:
                    break
                dotCount += 1
                numString += '.'
            else:
                numString += self.currentChar
            self.advance()

        if dotCount == 0:
            return Token(TT_INT, int(numString), posStart, self.pos)
        else:
            return Token(TT_FLOAT, float(numString), posStart, self.pos)

    def makeIdentifier(self):
        idStr = ''
        posStart = self.pos.copy()

        while self.currentChar != None and self.currentChar in LETTERS_DIGITS + '_':
            idStr += self.currentChar
            self.advance()

        tokenType = TT_KEYWORD if idStr in KEYWORDS else TT_IDENTIFIER
        return Token(tokenType, idStr, posStart, self.pos)

    def makeString(self):
        string = ''
        posStart = self.pos.copy()
        escapeCharacter = False
        self.advance()

        escapeCharacters = {
            'n': '\n',
            't': '\t'
        }

        while self.currentChar != None and (self.currentChar != '"' or escapeCharacter):
            if escapeCharacter:
                string += escapeCharacters.get(self.currentChar,
                                               self.currentChar)
            else:
                if self.currentChar == '\\':
                    escapeCharacter = True
                else:
                    string += self.currentChar
            self.advance()
            escapeCharacter = False

        self.advance()
        return Token(TT_STRING, string, posStart, self.pos)

    def makeMinusOrArrow(self):
        tokenType = TT_MINUS
        posStart = self.pos.copy()
        self.advance()

        if self.currentChar == '>':
            self.advance()
            tokenType = TT_ARROW

        return Token(tokenType, posStart=posStart, posEnd=self.pos)

    def makeNotEquals(self):
        posStart = self.pos.copy()
        self.advance()

        if self.currentChar == '=':
            self.advance()
            return Token(TT_NE, posStart=posStart, posEnd=self.pos), None

        self.advance()
        return None, ExpectedCharError(posStart, self.pos, "'=' (after '!')")

    def makeEquals(self):
        tokenType = TT_EQ
        posStart = self.pos.copy()
        self.advance()

        if self.currentChar == '=':
            self.advance()
            tokenType = TT_EE

        return Token(tokenType, posStart=posStart, posEnd=self.pos)

    def makeLessThan(self):
        tokenType = TT_LT
        posStart = self.pos.copy()
        self.advance()

        if self.currentChar == '=':
            self.advance()
            tokenType = TT_LTE

        return Token(tokenType, posStart=posStart, posEnd=self.pos)

    def makeGreaterThan(self):
        tokenType = TT_GT
        posStart = self.pos.copy()
        self.advance()

        if self.currentChar == '=':
            self.advance()
            tokenType = TT_GTE

        return Token(tokenType, posStart=posStart, posEnd=self.pos)
    def skip_comment(self):
        self.advance()

        while self.currentChar != '\n':
            self.advance()

        self.advance()

# --------------------------- Nodes ------------------------------
class NumberNode:
    def __init__(self, token):
        self.token = token
        self.posStart = self.token.posStart
        self.posEnd = token.posEnd

    def __repr__(self):
        return f'{self.token}'


class StringNode:
    def __init__(self, token):
        self.token = token
        self.posStart = self.token.posStart
        self.posEnd = token.posEnd

    def __repr__(self):
        return f'{self.token}'


class ListNode:
    def __init__(self, element_nodes, posStart, posEnd):
        self.element_nodes = element_nodes

        self.posStart = posStart
        self.posEnd = posEnd


class VarAccessNode:
    def __init__(self, varNameToken):
        self.varNameToken = varNameToken

        self.posStart = self.varNameToken.posStart
        self.posEnd = self.varNameToken.posEnd


class VarAssignNode:
    def __init__(self, varNameToken, valueNode):
        self.varNameToken = varNameToken
        self.valueNode = valueNode

        self.posStart = self.varNameToken.posStart
        self.posEnd = self.valueNode.posEnd


class BinOpNode:
    def __init__(self, leftNode, opToken, rightNode):
        self.leftNode = leftNode
        self.opToken = opToken
        self.rightNode = rightNode
        self.posStart = self.leftNode.posStart
        self.posEnd = self.rightNode.posEnd

    def __repr__(self):
        return f'({self.leftNode}, {self.opToken}, {self.rightNode})'


class UnaryOpNode:
    def __init__(self, opToken, node):
        self.opToken = opToken
        self.node = node
        self.posStart = self.opToken.posStart
        self.posEnd = node.posEnd

    def __repr__(self):
        return f'({self.opToken}, {self.node})'


class IfNode:
    def __init__(self, cases, else_case):
        self.cases = cases
        self.else_case = else_case

        self.posStart = self.cases[0][0].posStart
        self.posEnd = (
            self.else_case or self.cases[len(self.cases) - 1])[0].posEnd


class ForNode:
    def __init__(self, varNameToken, startValueNode, endValueNode, stepValueNode, bodyNode, should_return_null):
        self.varNameToken = varNameToken
        self.startValueNode = startValueNode
        self.endValueNode = endValueNode
        self.stepValueNode = stepValueNode
        self.bodyNode = bodyNode
        self.should_return_null = should_return_null

        self.posStart = self.varNameToken.posStart
        self.posEnd = self.bodyNode.posEnd


class WhileNode:
    def __init__(self, conditionNode, bodyNode, should_return_null):
        self.conditionNode = conditionNode
        self.bodyNode = bodyNode
        self.should_return_null = should_return_null

        self.posStart = self.conditionNode.posStart
        self.posEnd = self.bodyNode.posEnd


class FunDefNode:
    def __init__(self, varNameToken, argNameToken, bodyNode, should_auto_return):
        self.varNameToken = varNameToken
        self.argNameToken = argNameToken
        self.bodyNode = bodyNode
        self.should_auto_return = should_auto_return

        if self.varNameToken:
            self.posStart = self.varNameToken.posStart
        elif len(self.argNameToken) > 0:
            self.posStart = self.argNameToken[0].posStart
        else:
            self.posStart = self.bodyNode.posStart

        self.posEnd = self.bodyNode.posEnd


class CallNode:
    def __init__(self, nodeToCall, argNodes):
        self.nodeToCall = nodeToCall
        self.argNodes = argNodes

        self.posStart = self.nodeToCall.posStart

        if len(self.argNodes) > 0:
            self.posEnd = self.argNodes[len(self.argNodes) - 1].posEnd
        else:
            self.posEnd = self.nodeToCall.posEnd


class ReturnNode:
    def __init__(self, node_to_return, posStart, posEnd):
        self.node_to_return = node_to_return
        self.posStart = posStart
        self.posEnd = posEnd


class ContinueNode:
    def __init__(self, posStart, posEnd):
        self.posStart = posStart
        self.posEnd = posEnd


class BreakNode:
    def __init__(self, posStart, posEnd):
        self.posStart = posStart
        self.posEnd = posEnd


class ParseResult:
    def __init__(self):
        self.error = None
        self.node = None
        self.last_registered_advance_count = 0
        self.advanceCount = 0
        self.to_reverse_count = 0

    def registerAdvancment(self):
        self.last_registered_advance_count = 1
        self.advanceCount += 1

    def register(self, res):
        self.last_registered_advance_count = res.advanceCount
        self.advanceCount += res.advanceCount
        if res.error:
            self.error = res.error
        return res.node

    def try_register(self, res):
        if res.error:
            self.to_reverse_count = res.advanceCount
            return None
        return self.register(res)

    def success(self, node):
        self.node = node
        return self

    def failure(self, error):
        if not self.error or self.last_registered_advance_count == 0:
            self.error = error
        return self


# --------------------------- Parse ------------------------------
class Parser:
    def __init__(self, tokens):
        self.tokens = tokens
        self.tokenID = -1
        self.advance()

    def advance(self):
        self.tokenID += 1
        self.updateCurrentToken()
        return self.currentToken

    def reverse(self, amount=1):
        self.tokenID -= amount
        self.updateCurrentToken()
        return self.currentToken

    def updateCurrentToken(self):
        if self.tokenID >= 0 and self.tokenID < len(self.tokens):
            self.currentToken = self.tokens[self.tokenID]

    def parse(self):
        res = self.statements()
        if not res.error and self.currentToken.type != TT_EOF:
            return res.failure(InvalidSyntaxError(self.currentToken.posStart, self.currentToken.posEnd,
                                                  "Expected '+', '-', '*', '/', '^', '==', '!=', '<', '>', <=', '>=', '[' ,'AND' or 'OR'"))
        return res
###########

    def statements(self):
        res = ParseResult()
        statements = []
        posStart = self.currentToken.posStart.copy()

        while self.currentToken.type == TT_NEWLINE:
            res.registerAdvancment()
            self.advance()

        statement = res.register(self.statement())
        if res.error:
            return res
        statements.append(statement)

        more_statements = True

        while True:
            newline_count = 0
            while self.currentToken.type == TT_NEWLINE:
                res.registerAdvancment()
                self.advance()
                newline_count += 1
            if newline_count == 0:
                more_statements = False

            if not more_statements:
                break

            statement = res.try_register(self.statement())
            if not statement:
                self.reverse(res.to_reverse_count)
                more_statements = False
                continue
            statements.append(statement)

        return res.success(ListNode(
            statements,
            posStart,
            self.currentToken.posEnd.copy()
        ))

    def statement(self):
        res = ParseResult()
        posStart = self.currentToken.posStart.copy()

        if self.currentToken.matches(TT_KEYWORD, 'return'):
            res.registerAdvancment()
            self.advance()

            expr = res.try_register(self.expression())
            if not expr:
                self.reverse(res.to_reverse_count)
            return res.success(ReturnNode(expr, posStart, self.currentToken.posStart.copy()))

        if self.currentToken.matches(TT_KEYWORD, 'continue'):
            res.registerAdvancment()
            self.advance()
            return res.success(ContinueNode(posStart, self.currentToken.posStart.copy()))

        if self.currentToken.matches(TT_KEYWORD, 'break'):
            res.registerAdvancment()
            self.advance()
            return res.success(BreakNode(posStart, self.currentToken.posStart.copy()))

        expr = res.register(self.expression())
        if res.error:
            return res.failure(InvalidSyntaxError(
                self.currentToken.posStart, self.currentToken.posEnd,
                "Expected ']', 'var', 'if, 'for', 'while', 'return', 'continue', 'break', '[' , 'fun', int, float, identifier"
            ))
        return res.success(expr)

    def list_expression(self):
        res = ParseResult()
        element_nodes = []
        posStart = self.currentToken.posStart.copy()

        if self.currentToken.type != TT_LSQUARE:
            return res.failure(InvalidSyntaxError(
                self.currentToken.posStart, self.currentToken.posEnd,
                f"Expected '['"
            ))

        res.registerAdvancment()
        self.advance()

        if self.currentToken.type == TT_RSQUARE:
            res.registerAdvancment()
            self.advance()
        else:
            element_nodes.append(res.register(self.expression()))
            if res.error:
                return res.failure(InvalidSyntaxError(
                    self.currentToken.posStart, self.currentToken.posEnd,
                    "Expected ']', 'var', 'if, 'for', 'while', '[' , 'fun', int, float, identifier"
                ))

            while self.currentToken.type == TT_COMMA:
                res.registerAdvancment()
                self.advance()

                element_nodes.append(res.register(self.expression()))
                if res.error:
                    return res

            if self.currentToken.type != TT_RSQUARE:
                return res.failure(InvalidSyntaxError,
                                   self.currentToken.posStart, self.currentToken.posEnd, f"Expected a ',' or a ']'")

            res.registerAdvancment()
            self.advance()
        return res.success(ListNode(
            element_nodes,
            posStart,
            self.currentToken.posEnd.copy()
        ))

    def if_expr(self):
        res = ParseResult()
        all_cases = res.register(self.if_expr_cases('if'))
        if res.error:
            return res
        cases, else_case = all_cases
        return res.success(IfNode(cases, else_case))

    def if_expr_b(self):
        return self.if_expr_cases('elif')

    def if_expr_c(self):
        res = ParseResult()
        else_case = None
        if self.currentToken.matches(TT_KEYWORD, 'else'):
            res.registerAdvancment()
            self.advance()

            if self.currentToken.type == TT_NEWLINE:
                res.registerAdvancment()
                self.advance()

                statements = res.register(self.statements())
                if res.error:
                    return res
                else_case = (statements, True)

                if self.currentToken.matches(TT_KEYWORD, 'end'):
                    res.registerAdvancment()
                    self.advance()
                else:
                    return res.failure(InvalidSyntaxError(
                        self.currentToken.posStart, self.currentToken.posEnd,
                        "Expected 'end'"
                    ))
            else:
                expr = res.register(self.statement())
                if res.error:
                    return res
                else_case = (expr, False)

        return res.success(else_case)

    def if_expr_b_or_c(self):
        res = ParseResult()
        cases, else_case = [], None

        if self.currentToken.matches(TT_KEYWORD, 'elif'):
            all_cases = res.register(self.if_expr_b())
            if res.error:
                return res
            cases, else_case = all_cases
        else:
            else_case = res.register(self.if_expr_c())
            if res.error:
                return res

        return res.success((cases, else_case))

    def if_expr_cases(self, case_keyword):
        res = ParseResult()
        cases = []
        else_case = None

        if not self.currentToken.matches(TT_KEYWORD, case_keyword):
            return res.failure(InvalidSyntaxError(
                self.currentToken.posStart, self.currentToken.posEnd,
                f"Expected '{case_keyword}'"
            ))

        res.registerAdvancment()
        self.advance()

        condition = res.register(self.expression())
        if res.error:
            return res

        if not self.currentToken.matches(TT_KEYWORD, 'then'):
            return res.failure(InvalidSyntaxError(
                self.currentToken.posStart, self.currentToken.posEnd,
                f"Expected 'then'"
            ))

        res.registerAdvancment()
        self.advance()

        if self.currentToken.type == TT_NEWLINE:
            res.registerAdvancment()
            self.advance()

            statements = res.register(self.statements())
            if res.error:
                return res
            cases.append((condition, statements, True))

            if self.currentToken.matches(TT_KEYWORD, 'end'):
                res.registerAdvancment()
                self.advance()
            else:
                all_cases = res.register(self.if_expr_b_or_c())
                if res.error:
                    return res
                new_cases, else_case = all_cases
                cases.extend(new_cases)
        else:
            expr = res.register(self.statement())
            if res.error:
                return res
            cases.append((condition, expr, False))

            all_cases = res.register(self.if_expr_b_or_c())
            if res.error:
                return res
            new_cases, else_case = all_cases
            cases.extend(new_cases)

        return res.success((cases, else_case))

    def for_expr(self):
        res = ParseResult()

        if not self.currentToken.matches(TT_KEYWORD, 'for'):
            return res.failure(InvalidSyntaxError(
                self.currentToken.posStart, self.currentToken.posEnd,
                f"Expected 'for'"
            ))

        res.registerAdvancment()
        self.advance()

        if self.currentToken.type != TT_IDENTIFIER:
            return res.failure(InvalidSyntaxError(
                self.currentToken.posStart, self.currentToken.posEnd,
                f"Expected identifier"
            ))

        varName = self.currentToken
        res.registerAdvancment()
        self.advance()

        if self.currentToken.type != TT_EQ:
            return res.failure(InvalidSyntaxError(
                self.currentToken.posStart, self.currentToken.posEnd,
                f"Expected '='"
            ))

        res.registerAdvancment()
        self.advance()

        startValue = res.register(self.expression())
        if res.error:
            return res

        if not self.currentToken.matches(TT_KEYWORD, 'to'):
            return res.failure(InvalidSyntaxError(
                self.currentToken.posStart, self.currentToken.posEnd,
                f"Expected 'to'"
            ))

        res.registerAdvancment()
        self.advance()

        endValue = res.register(self.expression())
        if res.error:
            return res

        if self.currentToken.matches(TT_KEYWORD, 'step'):
            res.registerAdvancment()
            self.advance()

            stepValue = res.register(self.expression())
            if res.error:
                return res
        else:
            stepValue = None

        if not self.currentToken.matches(TT_KEYWORD, 'then'):
            return res.failure(InvalidSyntaxError(
                self.currentToken.posStart, self.currentToken.posEnd,
                f"Expected 'then'"
            ))

        res.registerAdvancment()
        self.advance()

        if self.currentToken.type == TT_NEWLINE:
            res.registerAdvancment()
            self.advance()

            body = res.register(self.statements())
            if res.error:
                return res

            if not self.currentToken.matches(TT_KEYWORD, 'end'):
                return res.failure(InvalidSyntaxError(
                    self.currentToken.posStart, self.currentToken.posEnd,
                    f"Expected 'end'"
                ))

            res.registerAdvancment()
            self.advance()

            return res.success(ForNode(varName, startValue, endValue, stepValue, body, True))

        body = res.register(self.statement())
        if res.error:
            return res
        return res.success(ForNode(varName, startValue, endValue, stepValue, body, False))

    def while_expr(self):
        res = ParseResult()

        if not self.currentToken.matches(TT_KEYWORD, 'while'):
            return res.failure(InvalidSyntaxError(
                self.currentToken.posStart, self.currentToken.posEnd,
                f"Expected 'while'"
            ))

        res.registerAdvancment()
        self.advance()

        condition = res.register(self.expression())
        if res.error:
            return res

        if not self.currentToken.matches(TT_KEYWORD, 'then'):
            return res.failure(InvalidSyntaxError(
                self.currentToken.posStart, self.currentToken.posEnd,
                f"Expected 'then'"
            ))

        res.registerAdvancment()
        self.advance()

        if self.currentToken.type == TT_NEWLINE:
            res.registerAdvancment()
            self.advance()

            body = res.register(self.statements())
            if res.error:
                return res

            if not self.currentToken.matches(TT_KEYWORD, 'end'):
                return res.failure(InvalidSyntaxError(
                    self.currentToken.posStart, self.currentToken.posEnd,
                    f"Expected 'end'"
                ))

            res.registerAdvancment()
            self.advance()

            return res.success(WhileNode(condition, body, True))

        body = res.register(self.statement())
        if res.error:
            return res

        return res.success(WhileNode(condition, body, False))

    def func_def(self):
        res = ParseResult()

        if not self.currentToken.matches(TT_KEYWORD, 'fun'):
            return res.failure(InvalidSyntaxError(
                self.currentToken.posStart, self.currentToken.posEnd,
                f"Expected 'fun'"
            ))

        res.registerAdvancment()
        self.advance()

        if self.currentToken.type == TT_IDENTIFIER:
            varNameToken = self.currentToken
            res.registerAdvancment()
            self.advance()
            if self.currentToken.type != TT_LPAREN:
                return res.failure(InvalidSyntaxError(
                    self.currentToken.posStart, self.currentToken.posEnd,
                    f"Expected '('"
                ))
        else:
            varNameToken = None
            if self.currentToken.type != TT_LPAREN:
                return res.failure(InvalidSyntaxError(
                    self.currentToken.posStart, self.currentToken.posEnd,
                    f"Expected identifier or '('"
                ))

        res.registerAdvancment()
        self.advance()
        argNameToken = []

        if self.currentToken.type == TT_IDENTIFIER:
            argNameToken.append(self.currentToken)
            res.registerAdvancment()
            self.advance()

            while self.currentToken.type == TT_COMMA:
                res.registerAdvancment()
                self.advance()

                if self.currentToken.type != TT_IDENTIFIER:
                    return res.failure(InvalidSyntaxError(
                        self.currentToken.posStart, self.currentToken.posEnd,
                        f"Expected identifier"
                    ))

                argNameToken.append(self.currentToken)
                res.registerAdvancment()
                self.advance()

            if self.currentToken.type != TT_RPAREN:
                return res.failure(InvalidSyntaxError(
                    self.currentToken.posStart, self.currentToken.posEnd,
                    f"Expected ',' or ')'"
                ))

            else:
                if self.currentToken.type != TT_RPAREN:
                    return res.failure(InvalidSyntaxError(
                        self.currentToken.posStart, self.currentToken.posEnd,
                        f"Expected identifier or ')'"
                    ))

            res.registerAdvancment()
            self.advance()

            if self.currentToken.type == TT_ARROW:
                res.registerAdvancment()
                self.advance()

                nodeToReturn = res.register(self.expression())
                if res.error:
                    return res

                return res.success(FunDefNode(
                    varNameToken,
                    argNameToken,
                    nodeToReturn,
                    True
                ))

            if self.currentToken.type != TT_NEWLINE:
                return res.failure(InvalidSyntaxError(
                    self.currentToken.posStart, self.currentToken.posEnd,
                    f"Expected '->' or NEWLINE"
                ))
            res.registerAdvancment()
            self.advance()

            nodeToReturn = res.register(self.statements())
            if res.error:
                return res

            if not self.currentToken.matches(TT_KEYWORD, 'end'):
                return res.failure(InvalidSyntaxError(
                    self.currentToken.posStart, self.currentToken.posEnd,
                    f"Expected 'end'"
                ))

            res.registerAdvancment()
            self.advance()

            return res.success(FunDefNode(
                varNameToken,
                argNameToken,
                nodeToReturn,
                False
            ))

    def call(self):
        res = ParseResult()
        atom = res.register(self.atom())
        if res.error:
            return res

        if self.currentToken.type == TT_LPAREN:
            res.registerAdvancment()
            self.advance()
            argNodes = []

            if self.currentToken.type == TT_RPAREN:
                res.registerAdvancment()
                self.advance()
            else:
                argNodes.append(res.register(self.expression()))
                if res.error:
                    return res.failure(InvalidSyntaxError(
                        self.currentToken.posStart, self.currentToken.posEnd,
                        "Expected ')', 'var', 'if, '[' ,'for', 'while', 'fun', int, float, identifier"
                    ))

                while self.currentToken.type == TT_COMMA:
                    res.registerAdvancment()
                    self.advance()

                    argNodes.append(res.register(self.expression()))
                    if res.error:
                        return res
                if self.currentToken.type != TT_RPAREN:
                    return res.failure(InvalidSyntaxError(
                        self.currentToken.posStart, self.currentToken.posEnd,
                        f"Expected',' or ')'"
                    ))

                res.registerAdvancment()
                self.advance()
            return res.success(CallNode(atom, argNodes))
        return res.success(atom)

    def atom(self):
        res = ParseResult()
        token = self.currentToken

        if token.type in (TT_INT, TT_FLOAT):
            res.registerAdvancment()
            self.advance()
            return res.success(NumberNode(token))

        elif token.type == (TT_STRING):
            res.registerAdvancment()
            self.advance()
            return res.success(StringNode(token))

        elif token.type == TT_IDENTIFIER:
            res.registerAdvancment()
            self.advance()
            return res.success(VarAccessNode(token))

        elif token.type == TT_LPAREN:
            res.registerAdvancment()
            self.advance()
            expression = res.register(self.expression())
            if res.error:
                return res
            if self.currentToken.type == TT_RPAREN:
                res.registerAdvancment()
                self.advance()
                return res.success(expression)
            else:
                return res.failure(
                    InvalidSyntaxError(self.currentToken.posStart, self.currentToken.posEnd, "Expected ')'"))
        elif token.type == TT_LSQUARE:
            list_expression = res.register(self.list_expression())
            if res.error:
                return res
            return res.success(list_expression)

        elif token.matches(TT_KEYWORD, 'if'):  # Looking for if Expression
            if_expr = res.register(self.if_expr())
            if res.error:
                return res
            return res.success(if_expr)

        elif token.matches(TT_KEYWORD, 'for'):  # Looking for for expression
            for_expr = res.register(self.for_expr())
            if res.error:
                return res
            return res.success(for_expr)

        elif token.matches(TT_KEYWORD, 'while'):  # Looking for while expression
            while_expr = res.register(self.while_expr())
            if res.error:
                return res
            return res.success(while_expr)

        elif token.matches(TT_KEYWORD, 'fun'):
            func_expr = res.register(self.func_def())
            if res.error:
                return res
            return res.success(func_expr)

        return res.failure(
            InvalidSyntaxError(token.posStart, token.posEnd, "Expected Integer or Float, Identifier, '+', '[' , '-' or '('"))

    def power(self):
        return self.binaryOperation(self.call, (TT_POW, ), self.factor)

    def factor(self):
        res = ParseResult()
        token = self.currentToken

        if token.type in (TT_PLUS, TT_MINUS):
            res.registerAdvancment()
            self.advance()
            factor = res.register(self.factor())
            if res.error:
                return res
            return res.success(UnaryOpNode(token, factor))

        return self.power()

    def term(self):
        return self.binaryOperation(self.factor, (TT_MUL, TT_DIV))

    def arithmeticExpression(self):
        return self.binaryOperation(self.term, (TT_PLUS, TT_MINUS))

    def comparisonExpression(self):
        res = ParseResult()

        if self.currentToken.matches(TT_KEYWORD, 'not'):
            operationToken = self.currentToken
            res.registerAdvancment()
            self.advance()

            node = res.register(self.comparisonExpression())
            if res.error:
                return res
            return res.success(UnaryOpNode(operationToken, node))

        node = res.register(
            self.binaryOperation(self.arithmeticExpression, (TT_EE, TT_NE, TT_LT, TT_GT, TT_LTE, TT_GTE)))

        if res.error:
            return res.failure(InvalidSyntaxError(self.currentToken.posStart, self.currentToken.posEnd,
                                                  "Expected Integer or Float, Identifier, '[' , '+', '-', '(', 'not'"))

        return res.success(node)

    def expression(self):
        res = ParseResult()
        if self.currentToken.matches(TT_KEYWORD, 'var'):
            res.registerAdvancment()
            self.advance()

            if self.currentToken.type != TT_IDENTIFIER:
                return res.failure(
                    InvalidSyntaxError(self.currentToken.posStart, self.currentToken.posEnd, 'Expected identifier'))
            varName = self.currentToken
            res.registerAdvancment()
            self.advance()

            if self.currentToken.type != TT_EQ:
                return res.failure(
                    InvalidSyntaxError(self.currentToken.posStart, self.currentToken.posEnd, "Expected '='"))

            res.registerAdvancment()
            self.advance()
            expression = res.register(self.expression())
            if res.error:
                return res
            return res.success(VarAssignNode(varName, expression))

        node = res.register(self.binaryOperation(
            self.comparisonExpression, ((TT_KEYWORD, 'and'), (TT_KEYWORD, 'or'))))

        if res.error:
            return res.failure(InvalidSyntaxError(self.currentToken.posStart, self.currentToken.posEnd,
                                                  "Expected 'VAR', Integer, Float, Identifier, '+', '[' ,'-', '(', or 'not'"))

        return res.success(node)

    def binaryOperation(self, func_a, operationTokens, func_b=None):
        if func_b == None:
            func_b = func_a
        res = ParseResult()
        left = res.register(func_a())
        if res.error:
            return res

        while self.currentToken.type in operationTokens or (
                self.currentToken.type, self.currentToken.value) in operationTokens:
            opToken = self.currentToken
            res.registerAdvancment()
            self.advance()
            right = res.register(func_b())
            if res.error:
                return res
            left = BinOpNode(left, opToken, right)
        return res.success(left)


class Context:
    def __init__(self, displayName, parent=None, parentEntryPosition=None):
        self.displayName = displayName
        self.parent = parent
        self.parentEntryPosition = parentEntryPosition
        self.symbolTable = None


class SymbolTable:
    def __init__(self, parent=None):
        self.symbols = {}
        self.parent = parent

    def get(self, name):
        value = self.symbols.get(name, None)
        if value == None and self.parent:
            return self.parent.get(name)
        return value

    def set(self, name, value):
        self.symbols[name] = value

    def remove(self, name):
        del self.symbols[name]


# --------------------------- Interpreter ------------------------------
class Interpreter:
    def visit(self, node, context):
        methodName = f'visit{type(node).__name__}'
        method = getattr(self, methodName, self.noVisitMethod)
        return method(node, context)

    def noVisitMethod(self, node, context):
        raise Exception(f'No visit{type(node).__name__} method defined')

    def visitNumberNode(self, node, context):
        return RunTimeResult().success(
            (Number(node.token.value).setContext(context).setPos(node.posStart, node.posEnd)))

    def visitStringNode(self, node, context):
        return RunTimeResult().success(
            String(node.token.value).setContext(
                context).setPos(node.posStart, node.posEnd)
        )

    def visitListNode(self, node, context):
        res = RunTimeResult()
        elements = []

        for element_node in node.element_nodes:
            elements.append(res.register(self.visit(element_node, context)))
            if res.should_return():
                return res

        return res.success(
            List(elements).setContext(context).setPos(
                node.posStart, node.posEnd)
        )

    def visitVarAccessNode(self, node, context):
        res = RunTimeResult()
        varName = node.varNameToken.value
        value = context.symbolTable.get(varName)

        if not value:
            return res.failure(RunTimeError(node.posStart, node.posEnd, f"'{varName}' is not defined", context))

        value = value.copy().setPos(node.posStart, node.posEnd).setContext(context)
        return res.success(value)

    def visitVarAssignNode(self, node, context):
        res = RunTimeResult()
        varName = node.varNameToken.value
        value = res.register(self.visit(node.valueNode, context))

        if res.should_return():
            return res
        context.symbolTable.set(varName, value)
        return res.success(value)

    def visitBinOpNode(self, node, context):
        res = RunTimeResult()
        left = res.register(self.visit(node.leftNode, context))
        if res.should_return():
            return res
        right = res.register(self.visit(node.rightNode, context))
        if res.should_return():
            return res

        if node.opToken.type == TT_PLUS:
            result, error = left.addTo(right)

        elif node.opToken.type == TT_MINUS:
            result, error = left.subBy(right)

        elif node.opToken.type == TT_MUL:
            result, error = left.multiplyBy(right)

        elif node.opToken.type == TT_DIV:
            result, error = left.divideBy(right)

        elif node.opToken.type == TT_POW:
            result, error = left.poweredBy(right)

        elif node.opToken.type == TT_EE:
            result, error = left.comparisonEQ(right)

        elif node.opToken.type == TT_NE:
            result, error = left.comparisonNE(right)

        elif node.opToken.type == TT_LT:
            result, error = left.comparisonLT(right)

        elif node.opToken.type == TT_GT:
            result, error = left.comparisonGT(right)

        elif node.opToken.type == TT_LTE:
            result, error = left.comparisonLTE(right)

        elif node.opToken.type == TT_GTE:
            result, error = left.comparisonGTE(right)

        elif node.opToken.matches(TT_KEYWORD, 'and'):
            result, error = left.andedBy(right)

        elif node.opToken.matches(TT_KEYWORD, 'or'):
            result, error = left.oredBy(right)

        if error:
            return res.failure(error)
        else:
            return res.success(result.setPos(node.posStart, node.posEnd))

    def visitUnaryOpNode(self, node, context):
        res = RunTimeResult()
        number = res.register(self.visit(node.node, context))
        if res.should_return():
            return res
        error = None

        if node.opToken.type == TT_MINUS:
            number, error = number.multiplyBy(Number(-1))
        elif node.opToken.matches(TT_KEYWORD, 'not'):
            number, error = number.notted()
        if error:
            return res.failure(error)
        else:
            return res.success(number.setPos(node.posStart, node.posEnd))

    def visitIfNode(self, node, context):
        res = RunTimeResult()

        for condition, expr, should_return_null in node.cases:
            condition_value = res.register(self.visit(condition, context))
            if res.should_return():
                return res

            if condition_value.is_true():
                expr_value = res.register(self.visit(expr, context))
                if res.should_return():
                    return res
                return res.success(Number.null if should_return_null else expr_value)

        if node.else_case:
            expr, should_return_null = node.else_case
            else_value = res.register(self.visit(expr, context))
            if res.should_return():
                return res
            return res.success(Number.null if should_return_null else else_value)

        return res.success(Number.null)

    def visitForNode(self, node, context):
        res = RunTimeResult()
        elements = []

        startValue = res.register(self.visit(node.startValueNode, context))
        if res.should_return():
            return res

        endValue = res.register(self.visit(node.endValueNode, context))
        if res.should_return():
            return res

        if node.stepValueNode:
            stepValue = res.register(self.visit(node.stepValueNode, context))
            if res.should_return():
                return res
        else:
            stepValue = Number(1)

        i = startValue.value

        if stepValue.value >= 0:
            def condition(): return i < endValue.value
        else:
            def condition(): return i > endValue.value

        while condition():
            context.symbolTable.set(node.varNameToken.value, Number(i))
            i += stepValue.value

            value = res.register(self.visit(node.bodyNode, context))
            if res.should_return() and res.loop_should_continue == False and res.loop_should_break == False:
                return res

            if res.loop_should_continue:
                continue

            if res.loop_should_break:
                break

            elements.append(value)

        return res.success(
            Number.null if node.should_return_null else
            List(elements).setContext(context).setPos(
                (node.posStart, node.posEnd))
        )

    def visitWhileNode(self, node, context):
        res = RunTimeResult()
        elements = []

        while True:
            condition = res.register(self.visit(node.conditionNode, context))
            if res.should_return():
                return res

            if not condition.is_true():
                break

            value = res.register(self.visit(node.bodyNode, context))
            if res.should_return() and res.loop_should_continue == False and res.loop_should_break == False:
                return res

            if res.loop_should_continue:
                continue

            if res.loop_should_break:
                break

            elements.append(value)

        return res.success(
            Number.null if node.should_return_null else
            List(elements).setContext(context).setPos(
                (node.posStart, node.posEnd))
        )

    def visitFunDefNode(self, node, context):
        res = RunTimeResult()

        func_name = node.varNameToken.value if node.varNameToken else None
        bodyNode = node.bodyNode
        argNames = [arg_name.value for arg_name in node.argNameToken]
        func_value = Function(func_name, bodyNode, argNames, node.should_auto_return).setContext(
            context).setPos(node.posStart, node.posEnd)

        if node.varNameToken:
            context.symbolTable.set(func_name, func_value)

        return res.success(func_value)

    def visitCallNode(self, node, context):
        res = RunTimeResult()
        args = []

        value_to_call = res.register(self.visit(node.nodeToCall, context))

        if res.should_return():
            return res
        value_to_call = value_to_call.copy().setPos(node.posStart, node.posEnd)

        for arg_node in node.argNodes:
            args.append(res.register(self.visit(arg_node, context)))
            if res.should_return():
                return res

        return_value = res.register(value_to_call.execute(args))
        if res.should_return():
            return res
        return_value = return_value.copy().setPos(
            node.posStart, node.posEnd).setContext(context)
        return res.success(return_value)

    def visitReturnNode(self, node, context):
        res = RunTimeResult()

        if node.node_to_return:
            value = res.register(self.visit(node.node_to_return, context))
            if res.should_return():
                return res
        else:
            value = Number.null

        return res.success_return(value)

    def visitContinueNode(self, node, context):
        return RunTimeResult().success_continue()

    def visitBreakNode(self, node, context):
        return RunTimeResult().success_break()


class RunTimeResult:
    def __init__(self):
        self.reset()

    def reset(self):
        self.value = None
        self.error = None
        self.func_return_value = None
        self.loop_should_continue = False
        self.loop_should_break = False

    def register(self, res):
        self.error = res.error
        self.func_return_value = res.func_return_value
        self.loop_should_continue = res.loop_should_continue
        self.loop_should_break = res.loop_should_break
        return res.value

    def success(self, value):
        self.reset()
        self.value = value
        return self

    def success_return(self, value):
        self.reset()
        self.func_return_value = value
        return self

    def success_continue(self):
        self.reset()
        self.loop_should_continue = True
        return self

    def success_break(self):
        self.reset()
        self.loop_should_break = True
        return self

    def failure(self, error):
        self.reset()
        self.error = error
        return self

    def should_return(self):
        return(
            self.error or
            self.func_return_value or
            self.loop_should_continue or
            self.loop_should_break
        )


class Value:
    def __init__(self):
        self.setPos()
        self.setContext()

    def setPos(self, posStart=None, posEnd=None):
        self.posStart = posStart
        self.posEnd = posEnd
        return self

    def setContext(self, context=None):
        self.context = context
        return self

    def addTo(self, other):
        return None, self.illegalOperation(other)

    def subBy(self, other):
        return None, self.illegalOperation(other)

    def multiplyBy(self, other):
        return None, self.illegalOperation(other)

    def divideBy(self, other):
        return None, self.illegalOperation(other)

    def poweredBy(self, other):
        return None, self.illegalOperation(other)

    def comparisonEQ(self, other):
        return None, self.illegalOperation(other)

    def comparisonNE(self, other):
        return None, self.illegalOperation(other)

    def comparisonLT(self, other):
        return None, self.illegalOperation(other)

    def comparisonGT(self, other):
        return None, self.illegalOperation(other)

    def comparisonLTE(self, other):
        return None, self.illegalOperation(other)

    def comparisonGTE(self, other):
        return None, self.illegalOperation(other)

    def andedBy(self, other):
        return None, self.illegalOperation(other)

    def oredBy(self, other):
        return None, self.illegalOperation(other)

    def notted(self, other):
        return None, self.illegalOperation(other)

    def execute(self, args):
        return None, self.illegalOperation()

    def copy(self):
        raise Exception('No copy method defined')

    def is_true(self):
        return False

    def illegalOperation(self, other=None):
        if not other:
            other = self
            return RunTimeError(
                self.posStart, other.posEnd,
                'Illegal operation',
                self.context
            )


class String(Value):
    def __init__(self, value):
        super().__init__()
        self.value = value

    def addTo(self, other):
        if isinstance(other, String):
            return String(self.value + other.value).setContext(self.context), None
        else:
            return None, Value.illegalOperation(self, other)

    def multedTo(self, other):
        if isinstance(other, String):
            return String(self.value * other.value).setContext(self.context), None
        else:
            return None, Value.illegalOperation(self, other)

    def is_true(self):
        return len(self.value) > 0

    def copy(self):
        copy = String(self.value)
        copy.setPos(self.posStart, self.posEnd)
        copy.setContext(self.context)
        return copy

    def __str__(self):
        return self.value

    def __repr__(self):
        return f'"{self.value}"'


class BasicFunction(Value):
    def __init__(self, name):
        super().__init__()
        self.name = name or "<anonymous>"

    def generateNewContext(self):
        newContext = Context(self.name, self.context, self.posStart)
        newContext.symbolTable = SymbolTable(newContext.parent.symbolTable)
        return newContext

    def checkArgs(self, argNames, arguments):
        res = RunTimeResult()

        if len(argNames) < len(arguments):
            return res.failure(RunTimeError(self.posStart, self.posEnd, f"{len(arguments) - len(argNames)} too high number of arguments past to 'self'", self.context))
        if len(argNames) > len(arguments):
            return res.failure(RunTimeError(self.posStart, self.posEnd, f"{len(arguments) - len(argNames)} too low number of arguments past to 'self'", self.context))
        return res.success(None)

    def popArguments(self, argNames, arguments, executionContext):
        for i in range(len(arguments)):
            argName = argNames[i]
            argValue = arguments[i]
            argValue.setContext(executionContext)
            executionContext.symbolTable.set(argName, argValue)

    def checkAndPopulateArgs(self, argNames, arguments, executionContext):
        res = RunTimeResult()
        res.register(self.checkArgs(argNames, arguments))
        if res.should_return():
            return res
        self.popArguments(argNames, arguments, executionContext)
        return res.success(None)


class Function(BasicFunction):
    def __init__(self, name, bodyNode, argNames, should_auto_return):
        super().__init__(name)
        self.bodyNode = bodyNode
        self.argNames = argNames
        self.should_auto_return = should_auto_return

    def execute(self, args):
        res = RunTimeResult()
        interpreter = Interpreter()
        executionContext = self.generateNewContext()

        res.register(self.checkAndPopulateArgs(
            self.argNames, args, executionContext))
        if res.should_return():
            return res

        value = res.register(interpreter.visit(
            self.bodyNode, executionContext))
        if res.should_return() and res.func_return_value == None:
            return res

        ret_value = (
            value if self.should_auto_return else None) or res.func_return_value or Number.null
        return res.success(ret_value)

    def copy(self):
        copy = Function(self.name, self.bodyNode,
                        self.argNames, self.should_auto_return)
        copy.setContext(self.context)
        copy.setPos(self.posStart, self.posEnd)
        return copy

    def __repr__(self):
        return f"<function {self.name}>"


class BuiltInFunction(BasicFunction):
    def __init__(self, name):
        super().__init__(name)

    def execute(self, arguments):
        res = RunTimeResult()
        executionContext = self.generateNewContext()
        methodName = f"execute{self.name}"
        method = getattr(self, methodName, self.noVisitMethod)
        res.register(self.checkAndPopulateArgs(method.argNames, arguments, executionContext))
        if res.should_return():
            return res

        returnValue = res.register(method(executionContext))
        if res.should_return():
            return res
        return res.success(returnValue)

    def noVisitMethod(self, node, context):
        raise Exception(f"No execute{self.name} method defined")

    def copy(self):
        copy = BuiltInFunction(self.name)
        copy.setContext(self.context)
        copy.setPos(self.posStart, self.posEnd)
        return copy

    def __repr__(self):
        return f"<built-in function {self.name}>"

    def executePrint(self, executionContext):
        print(str(executionContext.symbolTable.get('value')))
        return RunTimeResult().success(Number.null)
    executePrint.argNames = ["value"]
    def executeProfessor(self, executionContext):
        print(
		"█▀█ █▀█ █▀█ █▀▀ █▀▀ █▀ █▀ █▀█ █▀█   ▄▀█ █▄░█ ▀█▀ █▀█ █▄░█   █▀ █▀▀ █▀▄▀█ █▀▀ █▄░█ █▀▀ █░█ █▀▀ █▄░█ █▄▀ █▀█\n"+
		"█▀▀ █▀▄ █▄█ █▀░ ██▄ ▄█ ▄█ █▄█ █▀▄   █▀█ █░▀█ ░█░ █▄█ █░▀█   ▄█ ██▄ █░▀░█ ██▄ █░▀█ █▄▄ █▀█ ██▄ █░▀█ █░█ █▄█")
        return RunTimeResult().success(Number.null)
    executeProfessor.argNames = ["professor"]
    def executePrintRet(self, executionContext):
        return RunTimeResult().success(String(str(executionContext.symbolTable.get('value'))))
    executePrintRet.argNames = ["value"]

    def executeInput(self, executionContext):
        text = input()
        return RunTimeResult().success(String(text))
    executeInput.argNames = []

    def executeinputInt(self, executionContext):
        while True:
            text = input()
            try:
                number = int(text)
                break
            except ValueError:
                print(f"'{text}' has to be an integer")
                continue
        return RunTimeResult().success(Number(number))
    executeinputInt.argNames = []

    def executeClear(self, executionContext):
        os.system('cls' if os.name == 'nt' else 'clear')
        return RunTimeResult().success(Number.null)
    executeClear.argNames = []

    def executeisNumber(self, executionContext):
        isNumber = isinstance(
            executionContext.symbolTable.get("value"), Number)
        return RunTimeResult().success(Number.true if isNumber else Number.false)
    executeisNumber.argNames = ['value']

    def executeisString(self, executionContext):
        isString = isinstance(
            executionContext.symbolTable.get("value"), String)
        return RunTimeResult().success(Number.true if isString else Number.false)
    executeisString.argNames = ['value']

    def executeisList(self, executionContext):
        isList = isinstance(executionContext.symbolTable.get("value"), List)
        return RunTimeResult().success(Number.true if isList else Number.false)
    executeisList.argNames = ['value']

    def executeisFunction(self, executionContext):
        isFunction = isinstance(
            executionContext.symbolTable.get("value"), BasicFunction)
        return RunTimeResult().success(Number.true if isFunction else Number.false)
    executeisFunction.argNames = ['value']

    def executeAppend(self, executionContext):
        list_ = executionContext.symbolTable.get("list")
        value = executionContext.symbolTable.get("value")

        if not isinstance(list_, List):
            return RunTimeResult().failure(RunTimeError(self.posStart, self.posEnd, "List must be the first argument", executionContext))

        list_.elements.append(value)
        return RunTimeResult().success(Number.null)
    executeAppend.argNames = ['list', 'value']

    def executePop(self, executionContext):
        list_ = executionContext.symbolTable.get("list")
        index = executionContext.symbolTable.get("index")

        if not isinstance(list_, List):
            return RunTimeResult().failure(RunTimeError(self.posStart, self.posEnd, "List must be the first argument", executionContext))
        if not isinstance(index, Number):
            return RunTimeResult().failure(RunTimeError(self.posStart, self.posEnd, "Number must be second argument", executionContext))
        try:
            element = list_.elements.pop(index.value)
        except:
            return RunTimeResult().failure(RunTimeError(self.posStart, self.posEnd, "List index out of bounds, elements could not be removed", executionContext))
        return RunTimeResult().success(element)
    executePop.argNames = ['list', 'index']

    def executeExtend(self, executionContext):
        listA = executionContext.symbolTable.get("listA")
        listB = executionContext.symbolTable.get("listB")
        print(listA)
        print(listB)
        if not isinstance(listA, List):
            return RunTimeResult().failure(RunTimeError(self.posStart, self.posEnd, "Argument #1 has to be a list", executionContext))
        if not isinstance(listB, List):
            return RunTimeResult().failure(RunTimeError(self.posStart, self.posEnd, "Argument #2 has to be a list", executionContext))
        listA.elements.extend(listB.elements)
        return RunTimeResult().success(Number.null)
    executeExtend.argNames = ["listA", "listB"]

    def executeLen(self, executionContext):
        list_ = executionContext.symbolTable.get("list")

        if not isinstance(list_, List):
            return RunTimeResult().failure(RunTimeError(
				self.posStart, self.posEnd, "Argument must be list",executionContext
			))
        return RunTimeResult().success(Number(len(list_.elements)))
    executeLen.argNames = ['list']
    def executeRun(self, executionContext):
        fn = executionContext.symbolTable.get("fn")
        if not isinstance(fn, String):
            return RunTimeResult().failure(RunTimeError(self.posStart, self.posEnd, 'Argument must be string', executionContext))
        fn = fn.value

        try:
            with open(fn, 'r') as f:
                script = f.read()
        except Exception as e:
            return RunTimeResult().failure(RunTimeError(
                self.posStart, self.posEnd,
                f"Failed to load script \"{fn}\"\n" + str(e),
                executionContext
            ))
        _, error = run(fn, script)
        if error:
            return RunTimeResult().failure(RunTimeError(
                self.posStart, self.posEnd,
                f"Failed to finish executing Script \" {fn}\"\n" +
                error.as_string(), executionContext
            ))
        return RunTimeResult().success(Number.null)
    executeRun.argNames = ['fn']


BuiltInFunction.print = BuiltInFunction("Print")
BuiltInFunction.professor = BuiltInFunction("Professor")
BuiltInFunction.printRet = BuiltInFunction("PrintRet")
BuiltInFunction.input = BuiltInFunction("Input")
BuiltInFunction.inputInt = BuiltInFunction("inputInt")
BuiltInFunction.clear = BuiltInFunction("Clear")
BuiltInFunction.isNumber = BuiltInFunction("isNumber")
BuiltInFunction.isString = BuiltInFunction("isString")
BuiltInFunction.isList = BuiltInFunction("isList")
BuiltInFunction.isFunction = BuiltInFunction("isFunction")
BuiltInFunction.Append = BuiltInFunction("Append")
BuiltInFunction.Pop = BuiltInFunction("Pop")
BuiltInFunction.Extend = BuiltInFunction("Extend")
BuiltInFunction.Len = BuiltInFunction("Len")
BuiltInFunction.Run = BuiltInFunction("Run")


class Number(Value):
    def __init__(self, value):
        self.value = value
        self.setPos()
        self.setContext()

    def setPos(self, posStart=None, posEnd=None):
        self.posStart = posStart
        self.posEnd = posEnd
        return self

    def setContext(self, context=None):
        self.context = context
        return self

    def addTo(self, other):
        if isinstance(other, Number):
            return Number(self.value + other.value).setContext(self.context), None
        else:
            return None, Value.illegalOperation(self.posStart, other.posEnd)

    def subBy(self, other):
        if isinstance(other, Number):
            return Number(self.value - other.value).setContext(self.context), None
        else:
            return None, Value.illegalOperation(self.posStart, other.posEnd)

    def multiplyBy(self, other):
        if isinstance(other, Number):
            return Number(self.value * other.value).setContext(self.context), None
        else:
            return None, Value.illegalOperation(self.posStart, other.posEnd)

    def divideBy(self, other):
        if isinstance(other, Number):
            if other.value == 0:
                return None, RunTimeError(other.posStart, other.posEnd, "Division by zero", self.context)

            return Number(self.value / other.value).setContext(self.context), None
        else:
            return None, Value.illegalOperation(self.posStart, other.posEnd)

    def poweredBy(self, other):
        if isinstance(other, Number):
            return Number(self.value ** other.value).setContext(self.context), None
        else:
            return None, Value.illegalOperation(self.posStart, other.posEnd)

    def comparisonEQ(self, other):
        if isinstance(other, Number):
            return Number(int(self.value == other.value)).setContext(self.context), None
        else:
            return None, Value.illegalOperation(self.posStart, other.posEnd)

    def comparisonNE(self, other):
        if isinstance(other, Number):
            return Number(int(self.value != other.value)).setContext(self.context), None
        else:
            return None, Value.illegalOperation(self.posStart, other.posEnd)

    def comparisonLT(self, other):
        if isinstance(other, Number):
            return Number(int(self.value < other.value)).setContext(self.context), None
        else:
            return None, Value.illegalOperation(self.posStart, other.posEnd)

    def comparisonGT(self, other):
        if isinstance(other, Number):
            return Number(int(self.value > other.value)).setContext(self.context), None
        else:
            return None, Value.illegalOperation(self.posStart, other.posEnd)

    def comparisonLTE(self, other):
        if isinstance(other, Number):
            return Number(int(self.value <= other.value)).setContext(self.context), None
        else:
            return None, Value.illegalOperation(self.posStart, other.posEnd)

    def comparisonGTE(self, other):
        if isinstance(other, Number):
            return Number(int(self.value >= other.value)).setContext(self.context), None
        else:
            return None, Value.illegalOperation(self.posStart, other.posEnd)

    def andedBy(self, other):
        if isinstance(other, Number):
            return Number(int(self.value and other.value)).setContext(self.context), None
        else:
            return None, Value.illegalOperation(self.posStart, other.posEnd)

    def oredBy(self, other):
        if isinstance(other, Number):
            return Number(int(self.value or other.value)).setContext(self.context), None
        else:
            return None, Value.illegalOperation(self.posStart, other.posEnd)

    def notted(self):
        return Number(1 if self.value == 0 else 0).setContext(self.context), None

    def copy(self):
        copy = Number(self.value)
        copy.setPos(self.posStart, self.posEnd)
        copy.setContext(self.context)
        return copy

    def is_true(self):
        return self.value != 0

    def __repr__(self):
        return str(self.value)


Number.null = Number("Returned with 0")
Number.false = Number("false")
Number.true = Number("true")
Number.math_PI = Number(math.pi)


class List(Value):
    def __init__(self, elements):
        super().__init__()
        self.elements = elements

    def addTo(self, other):
        new_list = self.copy()
        new_list.elements.append(other.value)
        return new_list, None

    def subBy(self, other):
        if isinstance(other, Number):
            new_list = self.copy()
            try:
                new_list.elements.pop(other.value)
                return new_list, None
            except:
                return None, RunTimeError(other.posStart, other.posEnd,
                                          'Index out of bounds, Failed to remove element from the list',
                                          self.context
                                          )
        else:
            return None, Value.ilegal_operation(self, other)

    def divideBy(self, other):
        if isinstance(other, Number):
            try:
                return self.elements[other.value], None
            except:
                return None, RunTimeError(other.posStart, other.posEnd,
                                          'Index out of bounds, Failed to get element from the list',
                                          self.context
                                          )

    def multiplyBy(self, other):
        if isinstance(other, List):
            new_list = self.copy()
            new_list.elements.extend(other.elements)
            return new_list, None
        else:
            return None, Value.illegalOperation(self, other)

    def copy(self):
        copy = List(self.elements)
        copy.setPos(self.posStart, self.posEnd)
        copy.setContext(self.context)
        return copy

    def __str__(self):
        return ", ".join([str(x) for x in self.elements])

    def __repr__(self):
        return f'[{", ".join([str(x) for x in self.elements])}]'


# --------------------------- RUN ------------------------------
globalSymbolTable = SymbolTable()
globalSymbolTable.set("NULL", Number.null)
globalSymbolTable.set("false", Number.false)
globalSymbolTable.set("true", Number.true)
globalSymbolTable.set("PRINT", BuiltInFunction.print)
globalSymbolTable.set("PROFESSOR", BuiltInFunction.professor)
globalSymbolTable.set("PRINT_RET", BuiltInFunction.printRet)
globalSymbolTable.set("INPUT", BuiltInFunction.input)
globalSymbolTable.set("INPUT_INT", BuiltInFunction.inputInt)
globalSymbolTable.set("CLEAR", BuiltInFunction.clear)
globalSymbolTable.set("CLS", BuiltInFunction.clear)
globalSymbolTable.set("IS_NUM", BuiltInFunction.isNumber)
globalSymbolTable.set("IS_STR", BuiltInFunction.isString)
globalSymbolTable.set("IS_LIST", BuiltInFunction.isList)
globalSymbolTable.set("IS_FUN", BuiltInFunction.isFunction)
globalSymbolTable.set("APPEND", BuiltInFunction.Append)
globalSymbolTable.set("POP", BuiltInFunction.Pop)
globalSymbolTable.set("EXTEND", BuiltInFunction.Extend)
globalSymbolTable.set("LEN", BuiltInFunction.Len)
globalSymbolTable.set("RUN", BuiltInFunction.Run)
globalSymbolTable.set("MATH_PI", Number.math_PI)


def run(fileName, text):
    lexer = Lexer(fileName, text)
    tokens, error = lexer.makeTokens()
    if error:
        return None, error

    parser = Parser(tokens)
    tree = parser.parse()

    if tree.error:
        return None, tree.error

    interpreter = Interpreter()
    context = Context('<program>')
    context.symbolTable = globalSymbolTable
    result = interpreter.visit(tree.node, context)

    return result.value, result.error
