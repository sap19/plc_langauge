import PlcLang

while True:
    text = input('PlCLanguage > ')
    if text.strip() == "": continue
    result, error = PlcLang.run('<placeholder>', text)

    if error:
        print(error.as_string())

    elif result:
        if len(result.elements) == 1:
            print(repr(result.elements[0]))
        else:
            print(repr(result))
